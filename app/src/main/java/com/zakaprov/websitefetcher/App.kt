package com.zakaprov.websitefetcher

import android.app.Activity
import android.app.Application
import com.futuremind.daggerutils.AndroidComponentsInjector
import com.zakaprov.websitefetcher.injection.DaggerAppComponent
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

class App : Application(), HasActivityInjector {

    @Inject
    lateinit var dispatchingActivityInjector: DispatchingAndroidInjector<Activity>

    override fun onCreate() {
        super.onCreate()

        DaggerAppComponent.builder()
            .application(this)
            .build()
            .inject(this)
        AndroidComponentsInjector.init(this)
    }

    override fun activityInjector() = dispatchingActivityInjector
}
