package com.zakaprov.websitefetcher.injection

import android.content.Context
import com.zakaprov.websitefetcher.App
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ContextModule {

    @Provides
    @Singleton
    fun provideAppContext(app: App): Context = app.applicationContext
}
