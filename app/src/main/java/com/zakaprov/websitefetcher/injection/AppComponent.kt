package com.zakaprov.websitefetcher.injection

import com.zakaprov.websitefetcher.App
import com.zakaprov.websitefetcher.network.NetworkModule
import com.zakaprov.websitefetcher.cache.StoreModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton


@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        ViewModelFactoryModule::class,
        ActivityInjectorModule::class,
        ContextModule::class,
        NetworkModule::class,
        StoreModule::class
    ]
)
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: App): Builder

        fun build(): AppComponent
    }

    fun inject(app: App)
}
