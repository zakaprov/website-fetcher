package com.zakaprov.websitefetcher.injection

import com.zakaprov.websitefetcher.ui.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityInjectorModule {

    @ContributesAndroidInjector
    abstract fun contributeMainActivity(): MainActivity
}
