package com.zakaprov.websitefetcher.widgets

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.widget.EditText

class PrefixedEditText : EditText {

    constructor(context: Context?) : super(context) { init() }
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) { init() }
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) { init() }

    private var prefix: String? = null

    override fun onSelectionChanged(selStart: Int, selEnd: Int) {
        super.onSelectionChanged(selStart, selEnd)
        if (selStart < prefix?.length ?: 0) {
            setSelection(text.length)
        }
    }

    private fun init() {
        prefix = text.toString()
        setSelection(text.length)

        addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(text: Editable) { }

            override fun beforeTextChanged(text: CharSequence, start: Int, count: Int, after: Int) { }

            override fun onTextChanged(text: CharSequence, start: Int, before: Int, count: Int) {
                if (text.toString().length < prefix?.length ?: 0) {
                    setText(prefix)
                }
            }
        })
    }
}
