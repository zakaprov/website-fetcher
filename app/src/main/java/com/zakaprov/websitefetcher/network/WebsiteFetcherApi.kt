package com.zakaprov.websitefetcher.network

import io.reactivex.Single
import okhttp3.ResponseBody
import retrofit2.http.GET
import retrofit2.http.Url

interface WebsiteFetcherApi {

    @GET
    fun getUrl(@Url url: String): Single<ResponseBody>
}
