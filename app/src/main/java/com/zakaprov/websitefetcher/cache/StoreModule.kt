package com.zakaprov.websitefetcher.cache

import android.content.Context
import com.nytimes.android.external.fs3.SourcePersisterFactory
import com.nytimes.android.external.fs3.filesystem.FileSystemFactory
import com.nytimes.android.external.store3.base.Persister
import com.nytimes.android.external.store3.base.impl.BarCode
import com.nytimes.android.external.store3.base.impl.Store
import com.nytimes.android.external.store3.base.impl.StoreBuilder
import com.zakaprov.websitefetcher.network.WebsiteFetcherApi
import dagger.Module
import dagger.Provides
import okio.BufferedSource
import java.nio.charset.Charset
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

private const val PERSISTENT_CACHE_TTL = 10L
private val PERSISTENT_CACHE_TTL_UNIT = TimeUnit.MINUTES

@Module
class StoreModule {

    @Singleton
    @Provides
    fun provideStorePersister(context: Context): Persister<BufferedSource, BarCode> =
        SourcePersisterFactory.create(
            FileSystemFactory.create(context.cacheDir),
            PERSISTENT_CACHE_TTL,
            PERSISTENT_CACHE_TTL_UNIT
        )

    @Provides
    @Singleton
    fun provideStore(
        api: WebsiteFetcherApi,
        persister: Persister<BufferedSource, BarCode>
    ): Store<String, BarCode> = StoreBuilder.parsedWithKey<BarCode, BufferedSource, String>()
        .fetcher { api.getUrl(it.key).map { it.source() } }
        .parser { _, value -> value.readString(Charset.defaultCharset()) }
        .persister(persister)
        .networkBeforeStale()
        .open()
}
