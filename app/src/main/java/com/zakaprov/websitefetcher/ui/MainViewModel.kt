package com.zakaprov.websitefetcher.ui

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.nytimes.android.external.store3.base.impl.BarCode
import com.nytimes.android.external.store3.base.impl.Store
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

private const val URL_BAR_CODE_TYPE = "getSource"

class MainViewModel @Inject constructor(
    private val store: Store<String, BarCode>
) : ViewModel() {

    val state: LiveData<MainState>
        get() = mutableState

    private val mutableState = MutableLiveData<MainState>()
    private var storeDisposable: Disposable? = null

    fun getSource(url: String) {
        val urlBarcode = BarCode(URL_BAR_CODE_TYPE, url)

        storeDisposable = store.get(urlBarcode)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { mutableState.value = MainState.Loading }
            .subscribe ({
                mutableState.value = MainState.SourceFetched(it)
            }, {
                mutableState.value = MainState.Error(it)
            })
    }

    override fun onCleared() {
        super.onCleared()
        storeDisposable?.let {
            if (!it.isDisposed) it.dispose()
        }
    }
}
