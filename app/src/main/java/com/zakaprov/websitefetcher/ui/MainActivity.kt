package com.zakaprov.websitefetcher.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.futuremind.daggerutils.Injectable
import com.zakaprov.websitefetcher.R
import com.zakaprov.websitefetcher.extensions.gone
import com.zakaprov.websitefetcher.extensions.invisible
import com.zakaprov.websitefetcher.extensions.visible
import com.zakaprov.websitefetcher.ui.MainState.Error
import com.zakaprov.websitefetcher.ui.MainState.Loading
import com.zakaprov.websitefetcher.ui.MainState.SourceFetched
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.HttpException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import javax.inject.Inject

class MainActivity : AppCompatActivity(), Injectable {

    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel = ViewModelProviders.of(this, viewModelFactory)[MainViewModel::class.java]

        viewModel.state.observe(this, Observer { state ->
            when (state) {
                Loading -> showLoading()
                is SourceFetched -> showSource(state.source)
                is Error -> showError(state.error)
            }
        })

        mainButtonSend.setOnClickListener {
            viewModel.getSource(mainUrlField.text.toString())
        }
    }

    private fun showLoading() {
        mainButtonSend.isEnabled = false
        mainUrlField.isEnabled = false

        mainProgressBar.visible()
        mainErrorText.gone()
    }

    private fun showSource(source: String) {
        mainSourceText.text = source

        mainButtonSend.isEnabled = true
        mainUrlField.isEnabled = true

        mainProgressBar.invisible()
        mainErrorText.gone()
    }

    private fun showError(error: Throwable) {
        mainErrorText.text = getString(when (error) {
            is UnknownHostException -> R.string.main_error_unknown_host
            is HttpException -> {
                when (error.code()) {
                    404 -> R.string.main_error_not_found
                    500 -> R.string.main_error_server
                    else -> R.string.main_error_other
                }
            }
            is SocketTimeoutException -> R.string.main_error_timeout
            else -> R.string.main_error_other
        })

        mainButtonSend.isEnabled = true
        mainUrlField.isEnabled = true

        mainProgressBar.invisible()
        mainErrorText.visible()
    }
}
