package com.zakaprov.websitefetcher.ui

sealed class MainState {
    data class SourceFetched(val source: String) : MainState()
    data class Error(val error: Throwable) : MainState()
    object Loading : MainState()
}
