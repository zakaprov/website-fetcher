The project uses the following open-source libraries:
- Dagger,
- Retrofit,
- Store,
- RxJava.

The architecture is based on the MVVM pattern, making use of Google's Architecture Components.